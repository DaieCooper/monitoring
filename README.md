**In Russian**

Cтек exporters + Prometheus + Grafana

Разверните NGINX

Создать дашборд метрик NGINX exporter если необходимо наблюдать

Общее количество активных соединений
Общее количество запросов за все время
RPS
Сделать прокси на web-bee.ru. Показывать сколько запросов было которые туда проксировались.

**In English**

Exporters stack + Prometheus + Grafana

Deploy NGINX

Create a dashboard of NGINX exporter metrics if you need to monitor

Total number of active connections
Total number of requests for all time
R.P.S.
Make a proxy on web-bee.ru. Show how many requests there were that were proxied there.
